public class Axis{

  float startX;
  float startY;
  float endX;
  float endY;  
  
  String label;
  float max;
  float min;
  float currMax;
  float currMin;
  int axesMode;
  
  color labelColor  = color(0, 0, 0);
  color titleColor = color(255, 0, 0);
  color axesColor  = color(255, 255, 255, 100);

  // unselected, selected, top slider selected, bottom slider selected
  int nothingSelected  = 0;
  int dimensionSelected = 3;
  int topSlider = 2;
  int bottomSlider = 3;

  ElementsWindow titleElement;
  ElementsWindow axes;
  ElementsWindow maxSlider;
  ElementsWindow minSlider;

  public Axis(String label, float max, float min, float startX, float startY, float endX, float endY){
    this.label = label;
    this.max = max;
    this.min = min;
    this.currMax = max;
    this.currMin = min;
    this.axesMode = nothingSelected;
 
    
    this.startX = startX;
    this.startY = startY;
    this.endX = endX;
    this.endY = endY;

    float axisWidth = 10;
    float axisRangeX = this.startX - axisWidth / 2;
    float axisRangeY = this.startY;
    float axisHeight = this.endY - this.startY;
    this.axes = new ElementsWindow(axisRangeX, axisRangeY, axisWidth, axisHeight);
    
    float maxSliderWidth = axisWidth;    
    float maxSliderHeight = 8;
    float maxSliderX = axisRangeX;
    float maxSliderY = axisRangeY - maxSliderHeight;
    
    this.maxSlider = new ElementsWindow(maxSliderX, maxSliderY, maxSliderWidth, maxSliderHeight);

    float minSliderHeight = 8;
    float minSliderX = axisRangeX;
    float minSliderY = axisRangeY + axisHeight;
    float minSliderWidth = axisWidth;
    
    this.minSlider = new ElementsWindow(minSliderX, minSliderY, minSliderWidth, minSliderHeight);

    float titleWidth = textWidth(this.label);
    float titleX = this.startX - titleWidth / 2;
    float titleHeight = textAscent() + textDescent();
    float titleY = this.startY - 2 * titleHeight;
    this.titleElement = new ElementsWindow(titleX, titleY, titleWidth, titleHeight);
  }


  // getters and setters
  
  public String getLabel(){
    return this.label;
  }

  public float getMax(){
    return this.max;
  }

  public float getMin(){
    return this.max;
  }

  public float getCurMax(){
    return this.min;
  }
  
  public float getCurMin(){
    return this.max;
  }

  public float getStartX(){
    return this.startX;
  }

  public float getStartY(){
    return this.startX;
  }
  
  public float getEndX(){
    return this.startX;
  }
  
  public float getEndY(){
    return this.startX;
  }
  
  public float getY(float value){
    return map(value, this.min, this.max, this.endY, this.startY);
  }

  public float getLevel(float value){
    return map(value, this.min, this.max, 0, 1);
  }

  public boolean isBetweenAxis(float value){
    if(this.currMin <= value && value <= this.currMax)
      return true;
    else
      return false;
  }

  public boolean slide(float x, float y) {
    if(this.maxSlider.isIntersectingWith(x, y)) {
      this.axesMode = topSlider;
      return true;
    }
    
    else if(this.minSlider.isIntersectingWith(x, y)) {
      this.axesMode = bottomSlider;
      return true;
    }
    
    else if(this.axes.isIntersectingWith(x, y)){
      this.axesMode = dimensionSelected;
      return true;
    } 
    
    else {
      this.axesMode = nothingSelected;
      return false;
    }
  }
  
  public void moveSlider(float previousY, float currentY){
    if(this.axesMode == topSlider) {
      if(currentY <= this.minSlider.getY()) {
        if(this.startY <= currentY){
          this.currMax = map(currentY, this.startY, this.endY, this.max, this.min);
          this.maxSlider.setY(currentY - this.maxSlider.getHeight());
        }
        
        else {
          this.currMax = this.max;
          this.maxSlider.setY(this.startY - this.maxSlider.getHeight());
        }
        this.axes.setY(this.maxSlider.getY() + this.maxSlider.getHeight());
        this.axes.setHeight(this.minSlider.getY() - this.axes.getY());
      }
    }
    
    else if(this.axesMode == bottomSlider){
      if(currentY >= this.maxSlider.getY() + this.maxSlider.getHeight()){
        if(currentY <= this.endY){
          this.currMin = map(currentY, this.startY, this.endY, this.max, this.min);
          this.minSlider.setY(currentY);
        }
        
        else {
          this.currMin = this.min;
          this.minSlider.setY(this.endY);
        }
        this.axes.setHeight(this.minSlider.getY() - this.axes.getY());
      }
    }
    
    else if(this.axesMode == dimensionSelected){
      float yMove = currentY - previousY;
     
      // slide down
      if(yMove > 0){
        if(this.minSlider.getY() + yMove <= this.endY){
          this.minSlider.setY(this.minSlider.getY() + yMove);
          this.currMin = map(this.minSlider.getY(), this.startY, this.endY, this.max, this.min);
          this.axes.setY(this.axes.getY() + yMove);
          this.maxSlider.setY(this.maxSlider.getY() + yMove);
          this.currMax = map(this.maxSlider.getY() + this.maxSlider.getHeight(), this.startY, this.endY, this.max, this.min);
        }
      }
      
      // slide up
      else {
        if(this.maxSlider.getY() + this.maxSlider.getHeight() + yMove >= this.startY){
          this.maxSlider.setY(this.maxSlider.getY() + yMove);
          this.currMax = map(this.maxSlider.getY() + this.maxSlider.getHeight(), this.startY, this.endY, this.max, this.min);
          this.axes.setY(this.axes.getY() + yMove);
          this.minSlider.setY(this.minSlider.getY() + yMove);
          this.currMin = map(this.minSlider.getY(), this.startY, this.endY, this.max, this.min);
        }
      }
    }
  }


  public void draw(){
    this.draw(labelColor, axesColor);
  }

  
  public void draw(color titleColor, color rangeColor){
    fill(titleColor);
    textAlign(CENTER, TOP);
    text(this.label, this.titleElement.getX(), this.titleElement.getY());
    this.drawAxis();
    this.drawRange(rangeColor);
  }

  public void flipAxis() {
    
  startX = -startX;
  startY = -startY;
  endX = -endX;
  endY = -endY;  
  
  max = -max;
  min = -min;
  currMax = -currMax;
  currMin = -currMin;

  
  }
  
  
  public void drawAxis() {
    fill(0);
    stroke(0, 0, 0, 0);
    strokeWeight(2);
    
    // top line
    line(this.startX - this.axes.getWidth(), this.startY, this.startX + this.axes.getWidth(), this.startY);
    
    // bottom line
    line(this.endX - this.axes.getWidth(), this.endY, this.endX + this.axes.getWidth(), this.endY);
    
    textAlign(RIGHT, CENTER);

    // top text
    text((int)this.max, this.startX - this.axes.getWidth(), this.startY);
    // bottom text
    text((int)this.min, this.endX - this.axes.getWidth(), this.endY);
  }
  
  // draw range with color c
  private void drawRange(color c) {
    stroke(0, 0, 0);
    
    fill(200, 200, 200, 100);
    rect(this.startX - this.axes.getWidth() / 2, this.startY, this.axes.getWidth(), this.axes.getY() - this.startY);
    
    fill(c);
    rect(this.axes.getX(), this.axes.getY(), this.axes.getWidth(), this.axes.getHeight());
    
    fill(200, 200, 200, 50);
    rect(this.endX - this.axes.getWidth() / 2, this.axes.getY() + this.axes.getHeight(), this.axes.getWidth(), this.endY - (this.axes.getY() + this.axes.getHeight()));
    
    
    stroke(0);
    line(this.maxSlider.getX() - this.maxSlider.getWidth() / 2, this.maxSlider.getY() + this.maxSlider.getHeight(), this.maxSlider.getX() + this.maxSlider.getWidth() * 3 / 2, this.maxSlider.getY() + this.maxSlider.getHeight());

    line(this.minSlider.getX() - this.minSlider.getWidth() / 2, this.minSlider.getY(), this.minSlider.getX() + this.minSlider.getWidth() * 3 / 2, this.minSlider.getY());
    
    textAlign(RIGHT, CENTER);
    if(this.currMax != this.max)
      text((int)this.currMax, this.maxSlider.getX() - this.maxSlider.getWidth(), this.maxSlider.getY() + this.maxSlider.getHeight()); //currentMax
    if(this.currMin != this.min)
      text((int)this.currMin, this.minSlider.getX() - this.minSlider.getWidth(), this.minSlider.getY()); //currentMin
  }

}