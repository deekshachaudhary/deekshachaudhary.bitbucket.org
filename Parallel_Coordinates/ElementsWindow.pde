public class ElementsWindow {

  float elementX;
  float elementY;
  float elementWidth;
  float elementHeight;
  float elementCenterX;
  float elementCenterY;
  boolean isMouseOver;

  public ElementsWindow(){
    this(-1, -1, -1, -1);
  }
  
  public ElementsWindow(float elementX, float elementY, float elementWidth, float elementHeight){
    this.elementX = elementX;
    this.elementY = elementY;
    this.elementWidth = elementWidth;
    this.elementHeight = elementHeight;
    this.elementCenterX = this.elementX + this.elementWidth / 2;
    this.elementCenterY = this.elementY + this.elementHeight / 2;    
    this.isMouseOver = false;
  }

  
  public float getX(){
    return this.elementX;
  }
  
  public float getY(){
    return this.elementY;
  }
  
  public float getWidth(){
    return this.elementWidth;
  }
  
  public float getHeight(){
    return this.elementHeight;
  }
  
  public float getCenterX(){
    return this.elementCenterX;
  }
  
  public float getCenterY(){
    return this.elementCenterY;
  }
  
  public boolean isHighlighted(){
    return this.isMouseOver;
  }
  
  public void highlight(){
    this.isMouseOver = true;
  }
  

  public void setX(float elementX){
    this.elementX = elementX;
    this.elementCenterX = this.elementX + this.elementWidth / 2;
    this.elementCenterY = this.elementY + this.elementHeight / 2;
  }
  
  public void setY(float elementY){
    this.elementY = elementY;
    this.elementCenterX = this.elementX + this.elementWidth / 2;
    this.elementCenterY = this.elementY + this.elementHeight / 2;
  }
  
  public void setWidth(float elementWidth){
    this.elementWidth = elementWidth;
    this.elementCenterX = this.elementX + this.elementWidth / 2;
    this.elementCenterY = this.elementY + this.elementHeight / 2;
  }
  
  public void setHeight(float elementHeight){
    this.elementHeight = elementHeight;
    this.elementCenterX = this.elementX + this.elementWidth / 2;
    this.elementCenterY = this.elementY + this.elementHeight / 2;
  }
    

  public boolean isIntersectingWith(float x, float y) {
    if(this.elementX <= x && x <= this.elementX + this.elementWidth) {
      
      if(this.elementY <= y && y <= this.elementY + this.elementHeight)
        return true;
      else
        return false;
    }
    
    else {
      return false;
    }
  }
}