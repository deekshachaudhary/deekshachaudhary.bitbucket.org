public class MainClass extends ElementsWindow{

  ArrayList<String> labels;
  ArrayList<Elements> elements;

  HashMap<String, Integer> colorMap;
  String[] elementLabels;

  ArrayList<Axis> axes;
  Axis oldAxis;
  Axis newAxis;
  
  color[] rgbColors = {color(255, 0, 0), color(0, 255, 0), color(0, 0, 255)};

  public MainClass(ArrayList<String> labels, ArrayList<Elements> elements, float elementX, float elementY, float elementWidth, float elementHeight){
    super(elementX, elementY, elementWidth, elementHeight);
    this.elements = elements;
    this.labels = labels;    
    this.oldAxis = null;
    this.newAxis = null;
    this.axisElements();

    this.colorMap = new HashMap<String, Integer>();
    int tracker = 0;
    for(int i = 0; i < this.elements.size(); i++){
      String classLabel = this.elements.get(i).getLabel();
      if(this.colorMap.get(classLabel) == null){
        color lineColor;
        if(tracker < rgbColors.length) {
          lineColor = rgbColors[tracker];
          ++tracker;
        }
        
        else {
          lineColor = color((int)random(0, 250), (int)random(0, 250), (int)random(0, 250));
        }
        this.colorMap.put(classLabel, lineColor);
      }
    }
    
    Set<String> keys = colorMap.keySet();
    this.elementLabels = keys.toArray(new String[keys.size()]);
  }

  private void axisElements(){
    int numDimensions = this.labels.size() - 1;
 
    float startY = this.getY() + this.getHeight() * 0.1;
    float endY = startY + this.getHeight() * 0.7;
    
    float xOffset = this.getWidth() / (float)numDimensions;
    float x = this.getX() + xOffset / 2;
    
    this.axes = new ArrayList<Axis>();
    
    for(int i = 0; i < numDimensions; i++){
      String title = this.labels.get(i);
      float max = MIN_FLOAT;
      float min = MAX_FLOAT;
      for(int j = 0; j < this.elements.size(); j++){
        Elements element = this.elements.get(j);
        float dimension = element.dimensions.get(i);
        if(dimension > max)
          max = dimension;
        if(dimension < min)
          min = dimension;
      }
      this.axes.add(new Axis(title, max, min, x, startY, x, endY));
      x += xOffset;
    }
  }
  
  public void draw(){
    background(255);
    this.drawLines();
    this.drawLabels();

    for(int i = 0; i < this.axes.size(); i++){
      Axis axis = this.axes.get(i);
      if(this.oldAxis == axis)
        axis.draw(color(255, 0, 0) ,color(255, 255, 255, 60));
      else
        axis.draw();
    }
    
  }
  

  public void onMouseClicked(float x, float y){
    if(this.isIntersectingWith(x, y)){
      boolean isSelected = false;
      for(int i = 0; i < this.axes.size(); i++){
        Axis axis = this.axes.get(i);
        if(axis.titleElement.isIntersectingWith(x, y)){
          this.oldAxis = axis;
          isSelected = true;
          break;
        }
      }
      if(!isSelected)
        this.oldAxis = null;
    }
  }
  
  public void onMousePressed(float x, float y){
    for(int i = 0; i < this.axes.size(); i++){
      Axis axis = this.axes.get(i);
      if(axis.slide(x, y)){
        this.newAxis = axis;
        break;
      }
    }
  }



  private boolean isMouseOnLine(float pointX, float pointY, float lineX1, float lineY1, float lineX2, float lineY2){
    if(lineX1 < lineX2){
      if(pointX < lineX1 || lineX2 < pointX)
        return false;
      if(lineY1 < lineY2){
        if(pointY < lineY1 || lineY2 < pointY)
          return false;
      }
      else {
        if(pointY < lineY2 || lineY1 < pointY)
          return false;
      }
    }
    
    else {
      if(pointX < lineX2 || lineX1 < pointX)
        return false;
      if(lineY1 < lineY2){
        if(pointY < lineY1 || lineY2 < pointY)
          return false;
      }
      else {
        if(pointY < lineY2 || lineY1 < pointY)
          return false;
      }
    }
    float x1 = lineX2 - lineX1;
    float y1 = lineY2 - lineY1;
    float x2 = pointX - lineX1;
    float y2 = pointY - lineY1;


    float distance = abs(x1 * y2 - y1 * x2) / pow((lineX2 - lineX1) * (lineX2 - lineX1) + (lineY2 - lineY1) * (lineY2 - lineY1), 0.5f);
    if(distance >= 0 && distance <= 2)
      return true;
    else
      return false;
  }

  
  public void onMouseReleased(float x, float y){
    this.newAxis = null;
  }  
  
  public void onMouseMoved(float x, float y){
    for(int i = 0; i < this.elements.size(); i++){
      Elements element = this.elements.get(i);
      for(int j = 1; j < element.dimensions.size(); j++){
        Axis axis1 = this.axes.get(j - 1);
        float x1 = axis1.getStartX();
        float y1 = axis1.getY(element.dimensions.get(j - 1));
        Axis axis2 = this.axes.get(j);
        float x2 = axis2.getStartX();
        float y2 = axis2.getY(element.dimensions.get(j));
        if(isMouseOnLine(x, y, x1, y1, x2, y2)){
          element.highlight();
          break;
        }
        else {
          element.isHighlighted = false;
        }
      }
    }
  }
  
  public void onMouseDragged(float fromX, float fromY, float toX, float toY){
    if(this.newAxis != null)
      this.newAxis.moveSlider(fromY, toY);
  }
 
  
  private void drawLines(){
    for(int i = 0; i < this.elements.size(); i++){
      Elements element = this.elements.get(i);
      if(element.getIsHighlighted()){
        strokeWeight(2);
        stroke(0);
      }
      
      else {
        strokeWeight(1);
        boolean withinAxis = true;
        for(int j = 0; j < element.dimensions.size(); j++){
          Axis axis = this.axes.get(j);
          float dimension = element.dimensions.get(j);
          withinAxis = axis.isBetweenAxis(dimension);
          if(!withinAxis)
            break;
        }
        
        if(withinAxis){
          if(this.oldAxis == null){
            String classLabel = element.getLabel();
            color lineColor = this.colorMap.get(classLabel);
            stroke(lineColor);
          }
          else {
            int selectedAxisIndex = this.axes.indexOf(this.oldAxis);
            float targetFeature = element.dimensions.get(selectedAxisIndex);
            float level = this.oldAxis.getLevel(targetFeature);
            color lineColor = lerpColor(color(255, 0, 180), color(0, 255, 180), level);
            stroke(lineColor);
          }
        }
        
        else {
          stroke(200);
        }
      }
      for(int j = 1; j < element.dimensions.size(); j++){
        Axis axis1 = this.axes.get(j - 1);
        float x1 = axis1.getStartX();
        float y1 = axis1.getY(element.dimensions.get(j - 1));
        Axis axis2 = this.axes.get(j);
        float x2 = axis2.getStartX();
        float y2 = axis2.getY(element.dimensions.get(j));
        line(x1, y1, x2, y2);
      }
      if(element.getIsHighlighted()){
        textAlign(BOTTOM, CENTER);
        for(int j = 0; j < element.dimensions.size(); j++){
          float dimension = element.dimensions.get(j);
          Axis axis = this.axes.get(j);
          float x = axis.getStartX();
          float y = axis.getY(dimension);
          text(dimension, x, y);
        }
      }
    }
  }
  private void drawLabels(){
    float x = this.getX() + this.getWidth() * 0.1;
    float y = this.getY() + this.getHeight() * 0.9;
    float y1 = textAscent() + textDescent();

    textAlign(BOTTOM, CENTER);
    String label = this.labels.get(this.labels.size() - 1);

    x += textWidth(label + " ");
    strokeWeight(2.0f);
    
    for(int i = 0; i < this.elementLabels.length; i++){
      String classLabel = this.elementLabels[i];
      if(this.oldAxis == null){
        color lineColor = this.colorMap.get(classLabel);
        stroke(lineColor);
      }
      else {
        fill(180);
        stroke(180);
      }
      y += y1;
    }
  }
  
}


//public class MainClass extends ElementsWindow{

//  ArrayList<String> labels;
//  ArrayList<Elements> elements;

//  HashMap<String, Integer> colorMap;
//  String[] elementLabels;

//  ArrayList<Axis> axes;
//  Axis oldAxis;
//  Axis newAxis;

//  color[] rgbColors = {color(255, 0, 0), color(0, 255, 0), color(0, 0, 255)};

//  public MainClass(ArrayList<String> labels, ArrayList<Elements> elements, float elementX, float elementY, float elementWidth, float elementHeight){
//    super(elementX, elementY, elementWidth, elementHeight);
//    this.elements = elements;
//    this.labels = labels;    
//    this.oldAxis = null;
//    this.newAxis = null;
//    this.axisElements();

//    this.colorMap = new HashMap<String, Integer>();
//    int tracker = 0;
//    for(int i = 0; i < this.elements.size(); i++){
//      String classLabel = this.elements.get(i).getLabel();
//      if(this.colorMap.get(classLabel) == null){
//        color lineColor;
//        if(tracker < rgbColors.length) {
//          lineColor = rgbColors[tracker];
//          ++tracker;
//        }
        
//        else {
//          lineColor = color((int)random(0, 250), (int)random(0, 250), (int)random(0, 250));
//        }
//        this.colorMap.put(classLabel, lineColor);
//      }
//    }
    
//    Set<String> keys = colorMap.keySet();
//    this.elementLabels = keys.toArray(new String[keys.size()]);
//  }

//  private void axisElements(){
//    int numDimensions = this.labels.size() - 1;
 
//    float startY = this.getY() + this.getHeight() * 0.1;
//    float endY = startY + this.getHeight() * 0.7;
    
//    float xOffset = this.getWidth() / (float)numDimensions;
//    float x = this.getX() + xOffset / 2;
    
//    this.axes = new ArrayList<Axis>();
    
//    for(int i = 0; i < numDimensions; i++){
//      String title = this.labels.get(i);
//      float max = MIN_FLOAT;
//      float min = MAX_FLOAT;
//      for(int j = 0; j < this.elements.size(); j++){
//        Elements element = this.elements.get(j);
//        float dimension = element.dimensions.get(i);
//        if(dimension > max)
//          max = dimension;
//        if(dimension < min)
//          min = dimension;
//      }
//      this.axes.add(new Axis(title, max, min, x, startY, x, endY));
//      x += xOffset;
//    }
//  }
  
//  public void draw(){
//    background(255);
//    this.drawLines();
//    this.drawLabels();

//    for(int i = 0; i < this.axes.size(); i++){
//      Axis axis = this.axes.get(i);
//      if(this.oldAxis == axis)
//        axis.draw(color(255, 0, 0) ,color(255, 255, 255, 60));
//      else
//        axis.draw();
//    }
    
//}
  
//  public void onMouseClicked(float x, float y){
//    if(this.isIntersectingWith(x, y)){
//      boolean isSelected = false;
//      for(int i = 0; i < this.axes.size(); i++){
//        Axis axis = this.axes.get(i);
//        if(axis.titleElement.isIntersectingWith(x, y)){
//          this.oldAxis = axis;
//          isSelected = true;
//          break;
//        }
//      }
//      if(!isSelected)
//        this.oldAxis = null;
        
//    }
//  }
  
//  public void onMousePressed(float x, float y){
//    for(int i = 0; i < this.axes.size(); i++){
//      Axis axis = this.axes.get(i);
//      if(axis.slide(x, y)){
//        this.newAxis = axis;
//        break;
//      }
//    }
//  }



//  private boolean isMouseOnLine(float pointX, float pointY, float lineX1, float lineY1, float lineX2, float lineY2){
//    if(lineX1 < lineX2){
//      if(pointX < lineX1 || lineX2 < pointX)
//        return false;
//      if(lineY1 < lineY2){
//        if(pointY < lineY1 || lineY2 < pointY)
//          return false;
//      }
//      else {
//        if(pointY < lineY2 || lineY1 < pointY)
//          return false;
//      }
//    }
    
//    else {
//      if(pointX < lineX2 || lineX1 < pointX)
//        return false;
//      if(lineY1 < lineY2){
//        if(pointY < lineY1 || lineY2 < pointY)
//          return false;
//      }
//      else {
//        if(pointY < lineY2 || lineY1 < pointY)
//          return false;
//      }
//    }
//    float x1 = lineX2 - lineX1;
//    float y1 = lineY2 - lineY1;
//    float x2 = pointX - lineX1;
//    float y2 = pointY - lineY1;


//    float distance = abs(x1 * y2 - y1 * x2) / pow((lineX2 - lineX1) * (lineX2 - lineX1) + (lineY2 - lineY1) * (lineY2 - lineY1), 0.5f);
//    if(distance >= 0 && distance <= 2)
//      return true;
//    else
//      return false;
//  }

  
//  public void onMouseReleased(float x, float y){
//    this.newAxis = null;
//  }  
  
//  public void onMouseMoved(float x, float y){
//    for(int i = 0; i < this.elements.size(); i++){
//      Elements element = this.elements.get(i);
//      for(int j = 1; j < element.dimensions.size(); j++){
//        Axis axis1 = this.axes.get(j - 1);
//        float x1 = axis1.getStartX();
//        float y1 = axis1.getY(element.dimensions.get(j - 1));
//        Axis axis2 = this.axes.get(j);
//        float x2 = axis2.getStartX();
//        float y2 = axis2.getY(element.dimensions.get(j));
//        if(isMouseOnLine(x, y, x1, y1, x2, y2)){
//          element.highlight();
//          break;
//        }
//        else {
//          element.isHighlighted = false;
//        }
//      }
//    }
//  }
  
//  public void onMouseDragged(float fromX, float fromY, float toX, float toY){
//    if(this.newAxis != null)
//      this.newAxis.moveSlider(fromY, toY);
//  }
 
  
//  private void drawLines(){
//    for(int i = 0; i < this.elements.size(); i++){
//      Elements element = this.elements.get(i);
//      if(element.isHighlighted==true){
//        strokeWeight(2);
//        stroke(0, 0, 0);
//      }
      
//      else {
//        strokeWeight(1);
//        boolean withinAxis = true;
//        for(int j = 0; j < element.dimensions.size(); j++){
//          Axis axis = this.axes.get(j);
//          float dimension = element.dimensions.get(j);
//          withinAxis = axis.isBetweenAxis(dimension);
//          if(!withinAxis)
//            break;
//        }
        
//        if(withinAxis){
//          if(this.oldAxis == null){
//            String classLabel = element.getLabel();
//            color lineColor = this.colorMap.get(classLabel);
//            stroke(lineColor);
//          }
//          else {
//            int selectedAxisIndex = this.axes.indexOf(this.oldAxis);
//            float targetFeature = element.dimensions.get(selectedAxisIndex);
//            float level = this.oldAxis.getLevel(targetFeature);
//            color lineColor = lerpColor(color(255, 0, 180), color(0, 255, 180), level);
//            stroke(lineColor);
//          }
//        }
        
//        else {
//          stroke(200);
//        }
//      }
//      for(int j = 1; j < element.dimensions.size(); j++){
//        Axis axis1 = this.axes.get(j - 1);
//        float x1 = axis1.getStartX();
//        float y1 = axis1.getY(element.dimensions.get(j - 1));
//        Axis axis2 = this.axes.get(j);
//        float x2 = axis2.getStartX();
//        float y2 = axis2.getY(element.dimensions.get(j));
//        line(x1, y1, x2, y2);
//      }
//      if(element.isHighlighted == true){
//        textAlign(LEFT, CENTER);
//        for(int j = 0; j < element.dimensions.size(); j++){
//          float dimension = element.dimensions.get(j);
//          Axis axis = this.axes.get(j);
//          float x = axis.getStartX();
//          float y = axis.getY(dimension);
//          text(dimension, x, y);
//        }
//      }
//    }
//  }
//  private void drawLabels(){
//    float x = this.getX() + this.getWidth() * 0.1;
//    float y = this.getY() + this.getHeight() * 0.9;
//    float y1 = textAscent() + textDescent();

//    textAlign(LEFT, CENTER);
//    String label = this.labels.get(this.labels.size() - 1);

//  text(label, x, y);
//    x += textWidth(label + " ");
//    strokeWeight(2);
    
//    for(int i = 0; i < this.elementLabels.length; i++){
//      String classLabel = this.elementLabels[i];
//      if(this.oldAxis == null){
//        color lineColor = this.colorMap.get(classLabel);
//        stroke(lineColor);
//      }
//      else {
//        fill(180);
//        stroke(180);
//      }
//      y += y1;
//    }
//  }

  
//}