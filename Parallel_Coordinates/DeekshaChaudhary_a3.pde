//import java.util.*;
//PGraphics backBuffer;


//int canvasWidth = 800;
//int canvasHeight = 500;

//String[] lines;
//ArrayList<String> names;
//String[] values;

//MainClass mainClass;
//Box b;

//void setup(){
    
//  size(800, 500);
//  surface.setResizable(true);
//  b = new Box();

//  backBuffer = createGraphics(width, height);
  
  
  
//  // load data
//  String path = "wholesale_customers_data.csv";
//  lines = loadStrings(path);
//  names = new ArrayList<String>();
//  values = new String[lines.length-1];
  
//  // labels - first row of our file
//  ArrayList<String> firstLine = new ArrayList<String>();
  
//  // the remaining data
//  values = split(lines[0], ",");
//  for(int i = 0; i < values.length; i++) {
//    firstLine.add(values[i]);
//  }
  
//  ArrayList<Elements> elements = new ArrayList<Elements>();
//  String[] lines = loadStrings(path);
//  for(int i = 1; i < lines.length; i++){
//    String[] data = split(lines[i], ",");
    
//    ArrayList<Float> dimensions = new ArrayList<Float>();
    
//    for(int j = 0; j < data.length - 1; j++) {
//      dimensions.add(float(data[j]));
//    }
//    String elementsLabel = data[data.length - 1];
//    elements.add(new Elements(dimensions, elementsLabel));
//  }
  
  
//  mainClass = new MainClass(firstLine, elements, 0, 0, canvasWidth, canvasHeight);
//}

//void draw(){
//  mainClass.draw();
//  b.draw();
//}

//void mouseClicked(){
//  mainClass.onMouseClicked(mouseX, mouseY);
//}

//void mousePressed(){
//  mainClass.onMousePressed(mouseX, mouseY);
//  b.mousePressed();
//}

//void mouseReleased(){
//  mainClass.onMouseReleased(mouseX, mouseY);
//}

//void mouseMoved(){
//  mainClass.onMouseMoved(mouseX, mouseY);
//}

//void mouseDragged(){
//  mainClass.onMouseDragged(pmouseX, pmouseY, mouseX, mouseY);
//  b.mouseDragged();
//}



import java.util.*;

int canvasWidth = 800;
int canvasHeight = 500;

String[] lines;
ArrayList<String> names;
String[] values;

MainClass mainClass;

void setup(){
    
  size(800, 500);
  surface.setResizable(true);
    
  
  // load data
  String path = "wholesale_customers_data.csv";
  lines = loadStrings(path);
  names = new ArrayList<String>();
  values = new String[lines.length-1];
  
  // labels - first row of our file
  ArrayList<String> firstLine = new ArrayList<String>();
  
  // the remaining data
  values = split(lines[0], ",");
  for(int i = 0; i < values.length; i++) {
    firstLine.add(values[i]);
  }
  
  ArrayList<Elements> elements = new ArrayList<Elements>();
  String[] lines = loadStrings(path);
  for(int i = 1; i < lines.length; i++){
    String[] data = split(lines[i], ",");
    
    ArrayList<Float> dimensions = new ArrayList<Float>();
    
    for(int j = 0; j < data.length - 1; j++) {
      dimensions.add(float(data[j]));
    }
    String elementsLabel = data[data.length - 1];
    elements.add(new Elements(dimensions, elementsLabel));
  }
  
  
  mainClass = new MainClass(firstLine, elements, 0, 0, canvasWidth, canvasHeight);
}

void draw(){
  mainClass.draw();  
}

void mouseClicked(){
  mainClass.onMouseClicked(mouseX, mouseY);
}

void mousePressed(){
  mainClass.onMousePressed(mouseX, mouseY);
}

void mouseReleased(){
  mainClass.onMouseReleased(mouseX, mouseY);
}

void mouseMoved(){
  mainClass.onMouseMoved(mouseX, mouseY);
}

void mouseDragged(){
  mainClass.onMouseDragged(pmouseX, pmouseY, mouseX, mouseY);
}