
public class Elements {

  String label;
  ArrayList<Float> dimensions;
  boolean isHighlighted;
  
  public Elements(ArrayList<Float> dimensions, String label){
    this.dimensions = dimensions;
    this.label = label;
    this.isHighlighted = false;
  }

  public String getLabel(){
    return this.label;
  }

  public boolean getIsHighlighted(){
    return this.isHighlighted;
  }
  
  public void highlight(){
    this.isHighlighted = true;
  }

  //@Override
  public String toString(){
    String s = "";
    for(int i = 0; i < this.dimensions.size(); i++)
      s += this.dimensions.get(i) + ",";
    s += this.label + ",";
    s += this.isHighlighted;
    return s;
  }

}